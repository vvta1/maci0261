package AngajatiApp.repository;

import AngajatiApp.controller.DidacticFunction;
import AngajatiApp.model.Employee;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeMockTest {

    @Test
    void modifyEmployeeFunction() {
        EmployeeMock employeeMock = new EmployeeMock();
        Employee ionel = new Employee("Marius", "Pacuraru", "1234567890876", DidacticFunction.ASISTENT, 2500d);
        employeeMock.modifyEmployeeFunction(ionel, DidacticFunction.valueOf("TEACHER"));
        assertEquals(employeeMock.getEmployeeList().get(0).getFunction(), DidacticFunction.TEACHER);
    }

    @Test
    void notModifyEmployeeFunction() {
        EmployeeMock employeeMock = new EmployeeMock();
        List<Employee> oldList = employeeMock.getEmployeeList();
        employeeMock.modifyEmployeeFunction(null, DidacticFunction.valueOf("TEACHER"));
        List<Employee> updatedList = employeeMock.getEmployeeList();
        assertEquals(oldList, updatedList);
    }
}