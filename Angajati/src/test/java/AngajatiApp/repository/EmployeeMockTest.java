package AngajatiApp.repository;

import AngajatiApp.controller.DidacticFunction;
import AngajatiApp.model.Employee;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeMockTest {

    private Employee employee;
    EmployeeMock employeeMock;
    int employeeNumber;

    @BeforeEach
    void setUp() {
        employeeMock = new EmployeeMock();
        employee = new Employee();
        employee.setFirstName("Marius");
        employee.setLastName("Pacuraru");
        employee.setCnp("1234567890876");
        employee.setFunction(DidacticFunction.ASISTENT);
        employee.setSalary(2500.0);
        employee.setId(0);
        try {
            employeeNumber = employeeMock.getEmployeeList().size();
        } catch (Exception e) {
            System.out.println("Error in setUp");
        }
        System.out.println("setUp");
    }

    @AfterEach
    void tearDown() {
        System.out.println("tearDown");
    }

    @Test
    void addEmployeeTC1() {
        try {
            boolean isEmployeeAdded = employeeMock.addEmployee(employee);
            assertTrue(isEmployeeAdded);
            assertEquals(employeeNumber + 1, employeeMock.getEmployeeList().size());
        } catch (Exception e) {
            assert (false);
        }
    }

    @Test
    void addEmployeeTC2() {
        try {
            employee.setCnp("a234567890876");
            boolean isEmployeeAdded = employeeMock.addEmployee(employee);
            assertFalse(isEmployeeAdded);
            assertEquals(employeeNumber, employeeMock.getEmployeeList().size());
        } catch (Exception e) {
            assert (true);
        }
    }


    @Test
    void addEmployeeTC3() {
        try {
            employee.setCnp("");
            boolean isEmployeeAdded = employeeMock.addEmployee(employee);
            assertFalse(isEmployeeAdded);
            assertEquals(employeeNumber, employeeMock.getEmployeeList().size());
        } catch (Exception e) {
            assert (true);
        }
    }

    @Test
    void addEmployeeTC4() {
        try {
            employee.setCnp("123456789087");
            boolean isEmployeeAdded = employeeMock.addEmployee(employee);
            assertFalse(isEmployeeAdded);
            assertEquals(employeeNumber, employeeMock.getEmployeeList().size());
        } catch (Exception e) {
            assert (true);
        }
    }

    @Test
    void addEmployeeTC5() {
        try {
            employee.setCnp(null);
            boolean isEmployeeAdded = employeeMock.addEmployee(employee);
            assertFalse(isEmployeeAdded);
            assertEquals(employeeNumber, employeeMock.getEmployeeList().size());
        } catch (Exception e) {
            assert (true);
        }
    }

    @Test
    void addEmployeeTC6() {
        try {
            employee.setFirstName(null);
            boolean isEmployeeAdded = employeeMock.addEmployee(employee);
            assertFalse(isEmployeeAdded);
            assertEquals(employeeNumber, employeeMock.getEmployeeList().size());
        } catch (Exception e) {
            assert (true);
        }
    }

    @Test
    void addEmployeeTC7() {
        try {
            employee.setFirstName("");
            boolean isEmployeeAdded = employeeMock.addEmployee(employee);
            assertFalse(isEmployeeAdded);
            assertEquals(employeeNumber, employeeMock.getEmployeeList().size());
        } catch (Exception e) {
            assert (true);
        }
    }

    @Test
    void addEmployeeTC8() {
        try {
            employee.setFirstName("Mar");
            boolean isEmployeeAdded = employeeMock.addEmployee(employee);
            assertTrue(isEmployeeAdded);
            assertEquals(employeeNumber + 1, employeeMock.getEmployeeList().size());
        } catch (Exception e) {
            assert (false);
        }
    }

    @Test
    void addEmployeeTC9() {
        try {
            employee.setFirstName("Ma");
            boolean isEmployeeAdded = employeeMock.addEmployee(employee);
            assertFalse(isEmployeeAdded);
            assertEquals(employeeNumber, employeeMock.getEmployeeList().size());
        } catch (Exception e) {
            assert (true);
        }
    }

    @Test
    void addEmployeeTC10() {
        try {
            employee.setFirstName("Mari");
            boolean isEmployeeAdded = employeeMock.addEmployee(employee);
            assertTrue(isEmployeeAdded);
            assertEquals(employeeNumber + 1, employeeMock.getEmployeeList().size());
        } catch (Exception e) {
            assert (false);
        }
    }

    @Test
    void addEmployeeTC11() {
        try {
            employee.setCnp("12345678908761");
            boolean isEmployeeAdded = employeeMock.addEmployee(employee);
            assertFalse(isEmployeeAdded);
            assertEquals(employeeNumber, employeeMock.getEmployeeList().size());
        } catch (Exception e) {
            assert (true);
        }
    }

}