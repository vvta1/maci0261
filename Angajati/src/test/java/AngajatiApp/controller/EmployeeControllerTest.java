package AngajatiApp.controller;

import AngajatiApp.model.Employee;
import AngajatiApp.repository.EmployeeMock;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeControllerTest {

    private Employee employee;
    EmployeeController employeeController;
    int employeeNumber;

    @BeforeEach
    void setUp() {
        EmployeeMock employeeMock = new EmployeeMock();
        employeeController = new EmployeeController(employeeMock);
        employee = new Employee();
        employee.setCnp("1234567890876");
        employee.setFirstName("Marius");
        employee.setLastName("Pacuraru");
        employee.setFunction(DidacticFunction.ASISTENT);
        employee.setSalary(2500.00);
        employee.setId(0);
        try {
            employeeNumber = employeeController.getEmployeesList().size();
        } catch (Exception e) {
        }
        System.out.println("setUp");
    }

    @AfterEach
    void tearDown() {
        System.out.println("tearDown");
    }

    @Test
    void addEmployeeTC1() {
        try {
            employeeController.addEmployee(employee);
            assertEquals(employeeNumber+1, employeeController.getEmployeesList().size());
            assert (true);
        } catch (Exception e) {
            assert (false);
        }
    }

    @Test //nu merge
    void addEmployeeTC2() {
        try {
            employee.setCnp("a234567890876");
            employeeController.addEmployee(employee);
            assert (false);
        } catch (Exception e) {
            assert (true);
            try {
                assertEquals(employeeNumber, employeeController.getEmployeesList().size());
            } catch (Exception e2) {
            }
        }
    }

    @Test //nu merge
    void addEmployeeTC3(){
        try{
            employee.setCnp("");
            employeeController.addEmployee(employee);
            assert (false);
        }catch(Exception e) {
            assert (true);
            try {
                assertEquals(employeeNumber, employeeController.getEmployeesList().size());
            } catch (Exception e2) {
        }
        }
    }

    @Test //nu merge
    void addEmployeeTC4(){
        try{
            employee.setCnp("123456789087");
            employeeController.addEmployee(employee);
            assert (false);
        }catch (Exception e){
            try {
                assertEquals(employeeNumber, employeeController.getEmployeesList().size());
            }catch (Exception e2) {
            }
        }
    }

    @Test
    void addEmployeeTC5() {
        try {
            employee.setCnp(null);
            employeeController.addEmployee(employee);
            assert (false);
        } catch (Exception e) {
            assert (true);
            try {
                assertEquals(employeeNumber, employeeController.getEmployeesList().size());
            } catch (Exception e2) {
            }
        }
    }

    @Test
    void addEmployeeTC6() {
        try {
            employee.setFirstName(null);
            employeeController.addEmployee(employee);
            assert (false);
        } catch (Exception e) {
            assert (true);
            try {
                assertEquals(employeeNumber, employeeController.getEmployeesList().size());
            } catch (Exception e2) {
            }
        }
    }

    @Test //nu merge
    void addEmployeeTC7() {
        try {
            employee.setFirstName("");
            employeeController.addEmployee(employee);
            assert (false);
        } catch (Exception e) {
            assert (true);
            try {
                assertEquals(employeeNumber, employeeController.getEmployeesList().size());
            } catch (Exception e2) {
            }
        }
    }

    @Test //nu merge
    void addEmployeeTC8() {
        try {
            employee.setFirstName("Mar");
            employeeController.addEmployee(employee);
            assert (false);
        } catch (Exception e) {
            assert (true);
            try {
                assertEquals(employeeNumber+1, employeeController.getEmployeesList().size());
            } catch (Exception e2) {
            }
        }
    }

    @Test //nu merge
    void addEmployeeTC9() {
        try {
            employee.setFirstName("Ma");
            employeeController.addEmployee(employee);
            assert (false);
        } catch (Exception e) {
            assert (true);
            try {
                assertEquals(employeeNumber, employeeController.getEmployeesList().size());
            } catch (Exception e2) {
            }
        }
    }

    @Test //nu merge
    void addEmployeeTC10() {
        try {
            employee.setFirstName("Mari");
            employeeController.addEmployee(employee);
            assert (false);
        } catch (Exception e) {
            assert (true);
            try {
                assertEquals(employeeNumber+1, employeeController.getEmployeesList().size());
            } catch (Exception e2) {
            }
        }
    }

    @Test //nu merge
    void addEmployeeTC11() {
        try {
            employee.setCnp("12345678908761");
            employeeController.addEmployee(employee);
            assert (false);
        } catch (Exception e) {
            assert (true);
            try {
                assertEquals(employeeNumber, employeeController.getEmployeesList().size());
            } catch (Exception e2) {
            }
        }
    }

}