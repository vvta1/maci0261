package AngajatiApp.repository;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import AngajatiApp.model.AgeCriteria;
import AngajatiApp.controller.DidacticFunction;
import AngajatiApp.model.Employee;
import AngajatiApp.validator.EmployeeException;
import AngajatiApp.validator.EmployeeValidator;
import AngajatiApp.model.SalaryCriteria;

public class EmployeeImpl implements EmployeeRepositoryInterface {

    private static final String ERROR_WHILE_READING_MSG = "Error while reading: ";
    private final String employeeDBFile = "employeeDB/employees.txt";
    private final String tempFile = "employeeDB/employeesTemp.txt";
    private EmployeeValidator employeeValidator = new EmployeeValidator();
    private List<Employee> employeeList = new ArrayList<>();

    public EmployeeImpl() {
        employeeList = loadEmployeesFromFile();
    }

    @Override
    public boolean addEmployee(Employee employee) {
        if (!employeeValidator.isValid(employee)) {
            return false;
        }

        if (employeeList.isEmpty()) {
            employee.setId(0);
        } else {
            Employee lastEmployee = employeeList.get(employeeList.size() - 1);
            employee.setId(lastEmployee.getId() + 1);
        }

        BufferedWriter bw = null;
        try {
            bw = new BufferedWriter(new FileWriter(employeeDBFile, true));
            bw.write(employee.toString());
            bw.newLine();
            bw.close();
            employeeList.add(employee);
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean modifyEmployeeFunction(int idEmployee, DidacticFunction newFunction) {
        Employee employee = findEmployeeById(idEmployee);
        int employeeIndex = employeeList.indexOf(employee);
        if (employeeIndex >= 0) {
            employeeList.get(employeeIndex).setFunction(newFunction);
            boolean fileUpdated = updateFileSource();
            return fileUpdated;
        }
        return false;
    }

    private boolean updateFileSource() {
        try {
            BufferedWriter fileWriter = new BufferedWriter(new FileWriter(tempFile));
            String temp;
            for (Employee employee : employeeList) {
                fileWriter.write(employee.toString());
                fileWriter.newLine();
            }
            fileWriter.close();

            File initialFile = new File(employeeDBFile);
            File newFile = new File(tempFile);

            initialFile.delete();
            newFile.renameTo(initialFile);

            return true;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return false;
    }

    private List<Employee> loadEmployeesFromFile() {
        final List<Employee> list = new ArrayList<Employee>();
        try (BufferedReader br = new BufferedReader(new FileReader(employeeDBFile));) {
            String line;
            int counter = 0;
            while ((line = br.readLine()) != null) {
                try {
                    final Employee employee = Employee.getEmployeeFromString(line);
                    list.add(employee);
                } catch (EmployeeException ex) {
                    System.err.println(ERROR_WHILE_READING_MSG + ex.toString());
                }
            }
        } catch (IOException e) {
            System.err.println(ERROR_WHILE_READING_MSG + e);
        }
        return list;
    }

    @Override
    public List<Employee> getEmployeeList() {
        return employeeList;
    }

    @Override
    public List<Employee> getEmployeeByCriteria() {
        List<Employee> employeeSortedList = new ArrayList<Employee>(employeeList);
        Collections.copy(employeeSortedList, employeeList);
        Collections.sort(employeeSortedList, new AgeCriteria());
        //System.out.println(employeeSortedList);
        Collections.sort(employeeSortedList, new SalaryCriteria());
        //System.out.println(employeeSortedList);
        return employeeSortedList;
    }

    @Override
    public Employee findEmployeeById(int idOldEmployee) {
        for (Employee employee : employeeList) {
            if (employee.getId() == idOldEmployee) {
                return employee;
            }
        }
        return null;
    }

}
